/*
Module serpents is a Wrapper around spf13/cobra and spf13/viper

It intends to allow easier and less error-prone flag/config-option duos declarations, without replacing either tool.

Example usage:

0) Import serpents

	$ go get codeberg.org/vlbeaudoin/serpents

1) Declare a config struct.

In this example, it contains fields `Port` and `Banner.Motd`.

Note: all fields will need to be registered to be updated in calls to
Config(). This is done in step 3).

	type config struct {
	  Banner struct {
	    Motd string
	  }

	  Port int
	}

2) Declare the getter for the current config.

It uses viper.Unmarshal, giving precedence to command-line flags over viper
config values.

	func Config() (cfg config, err error) {
	    return cfg, viper.Unmarshal(&cfg)
	}

3) Register the flags.

This is done on a *pflag.FlagSet, which is generally gotten from either:

- (*cobra.Command).Flags()
- (*cobra.Command).PersistentFlags()

	func RegisterFlags(flagSet *pflag.FlagSet) error {
	  if err := serpents.Int(flagSet,
	    "port", "port", "application port", 1312); err != nil {
	    return err
	  }

	  if err := serpents.String(flagSet,
	    "banner.motd", "banner-motd", "Message-Of-The-Day", "Hello, world!"); err != nil {
	    return err
	  }

	  return nil
	}

After calling `RegisterFlags()` on the proper flagset (for example,
`rootCmd.PersistentFlags()`), you will be able to call `Config()` to get
values registered by cobra and viper for your specific flags.
*/
package serpents

import (
	"fmt"
	"net"
	"time"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// descriptionWithConfig concatenates together a cobraDescription and viperName
func descriptionWithConfig(cobraDescription, viperName string) string {
	return fmt.Sprintf("%s (config: '%s')", cobraDescription, viperName)
}

// Bool registers a new cobra Bool flag and associated viper config option
func Bool(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault bool, cobraDescription string) error {
	flagSet.Bool(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BoolP registers a new cobra Bool pflag and associated viper config option
func BoolP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault bool, cobraDescription string) error {
	flagSet.BoolP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BoolSlice registers a new cobra BoolSlice flag and associated viper config option
func BoolSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []bool, cobraDescription string) error {
	flagSet.BoolSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BoolSliceP registers a new cobra BoolSlice pflag and associated viper config option
func BoolSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []bool, cobraDescription string) error {
	flagSet.BoolSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BytesBase64 registers a new cobra BytesBase64 flag and associated viper config option
func BytesBase64(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []byte, cobraDescription string) error {
	flagSet.BytesBase64(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BytesBase64P registers a new cobra BytesBase64 pflag and associated viper config option
func BytesBase64P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []byte, cobraDescription string) error {
	flagSet.BytesBase64P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BytesHex registers a new cobra BytesHex flag and associated viper config option
func BytesHex(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []byte, cobraDescription string) error {
	flagSet.BytesHex(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// BytesHexP registers a new cobra BytesHex pflag and associated viper config option
func BytesHexP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []byte, cobraDescription string) error {
	flagSet.BytesHexP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Count registers a new cobra Count flag and associated viper config option
func Count(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDescription string) error {
	flagSet.Count(cobraName, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// CountP registers a new cobra Count pflag and associated viper config option
func CountP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDescription string) error {
	flagSet.CountP(cobraName, cobraShortName, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Duration registers a new cobra Duration flag and associated viper config option
func Duration(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault time.Duration, cobraDescription string) error {
	flagSet.Duration(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// DurationP registers a new cobra Duration pflag and associated viper config option
func DurationP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault time.Duration, cobraDescription string) error {
	flagSet.DurationP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// DurationSlice registers a new cobra DurationSlice flag and associated viper config option
func DurationSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []time.Duration, cobraDescription string) error {
	flagSet.DurationSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// DurationSliceP registers a new cobra DurationSlice pflag and associated viper config option
func DurationSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []time.Duration, cobraDescription string) error {
	flagSet.DurationSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float32 registers a new cobra Float32 flag and associated viper config option
func Float32(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault float32, cobraDescription string) error {
	flagSet.Float32(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float32P registers a new cobra Float32 pflag and associated viper config option
func Float32P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault float32, cobraDescription string) error {
	flagSet.Float32P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float32Slice registers a new cobra Float32Slice flag and associated viper config option
func Float32Slice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []float32, cobraDescription string) error {
	flagSet.Float32Slice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float32SliceP registers a new cobra Float32Slice pflag and associated viper config option
func Float32SliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []float32, cobraDescription string) error {
	flagSet.Float32SliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float64 registers a new cobra Float64 flag and associated viper config option
func Float64(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault float64, cobraDescription string) error {
	flagSet.Float64(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float64P registers a new cobra Float64 pflag and associated viper config option
func Float64P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault float64, cobraDescription string) error {
	flagSet.Float64P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float64Slice registers a new cobra Float64Slice flag and associated viper config option
func Float64Slice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []float64, cobraDescription string) error {
	flagSet.Float64Slice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Float64SliceP registers a new cobra Float64Slice pflag and associated viper config option
func Float64SliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []float64, cobraDescription string) error {
	flagSet.Float64SliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IP registers a new cobra IP flag and associated viper config option
func IP(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault net.IP, cobraDescription string) error {
	flagSet.IP(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPMask registers a new cobra IPMask flag and associated viper config option
func IPMask(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault net.IPMask, cobraDescription string) error {
	flagSet.IPMask(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPMaskP registers a new cobra IPMask pflag and associated viper config option
func IPMaskP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault net.IPMask, cobraDescription string) error {
	flagSet.IPMaskP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPNet registers a new cobra IPNet flag and associated viper config option
func IPNet(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault net.IPNet, cobraDescription string) error {
	flagSet.IPNet(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPNetP registers a new cobra IPNet pflag and associated viper config option
func IPNetP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault net.IPNet, cobraDescription string) error {
	flagSet.IPNetP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPP registers a new cobra IP pflag and associated viper config option
func IPP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault net.IP, cobraDescription string) error {
	flagSet.IPP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPSlice registers a new cobra IPSlice flag and associated viper config option
func IPSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []net.IP, cobraDescription string) error {
	flagSet.IPSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IPSliceP registers a new cobra IPSlice pflag and associated viper config option
func IPSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []net.IP, cobraDescription string) error {
	flagSet.IPSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int registers a new cobra Int flag and associated viper config option
func Int(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault int, cobraDescription string) error {
	flagSet.Int(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int16 registers a new cobra Int16 flag and associated viper config option
func Int16(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault int16, cobraDescription string) error {
	flagSet.Int16(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int16P registers a new cobra Int16 pflag and associated viper config option
func Int16P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault int16, cobraDescription string) error {
	flagSet.Int16P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int32 registers a new cobra Int32 flag and associated viper config option
func Int32(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault int32, cobraDescription string) error {
	flagSet.Int32(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int32P registers a new cobra Int32 pflag and associated viper config option
func Int32P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault int32, cobraDescription string) error {
	flagSet.Int32P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int32Slice registers a new cobra Int32Slice flag and associated viper config option
func Int32Slice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []int32, cobraDescription string) error {
	flagSet.Int32Slice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int32SliceP registers a new cobra Int32Slice pflag and associated viper config option
func Int32SliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []int32, cobraDescription string) error {
	flagSet.Int32SliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int64 registers a new cobra Int64 flag and associated viper config option
func Int64(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault int64, cobraDescription string) error {
	flagSet.Int64(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int64P registers a new cobra Int64 pflag and associated viper config option
func Int64P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault int64, cobraDescription string) error {
	flagSet.Int64P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int64Slice registers a new cobra Int64Slice flag and associated viper config option
func Int64Slice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []int64, cobraDescription string) error {
	flagSet.Int64Slice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int64SliceP registers a new cobra Int64Slice pflag and associated viper config option
func Int64SliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []int64, cobraDescription string) error {
	flagSet.Int64SliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int8 registers a new cobra Int8 flag and associated viper config option
func Int8(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault int8, cobraDescription string) error {
	flagSet.Int8(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Int8P registers a new cobra Int8 pflag and associated viper config option
func Int8P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault int8, cobraDescription string) error {
	flagSet.Int8P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IntP registers a new cobra Int pflag and associated viper config option
func IntP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault int, cobraDescription string) error {
	flagSet.IntP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IntSlice registers a new cobra IntSlice flag and associated viper config option
func IntSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []int, cobraDescription string) error {
	flagSet.IntSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// IntSliceP registers a new cobra IntSlice pflag and associated viper config option
func IntSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []int, cobraDescription string) error {
	flagSet.IntSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// String registers a new cobra String flag and associated viper config option
func String(flagSet *pflag.FlagSet, viperName, cobraName, cobraDefault, cobraDescription string) error {
	flagSet.String(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringArray registers a new cobra StringArray flag and associated viper config option
func StringArray(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []string, cobraDescription string) error {
	flagSet.StringArray(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringArrayP registers a new cobra StringArray pflag and associated viper config option
func StringArrayP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []string, cobraDescription string) error {
	flagSet.StringArrayP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringP registers a new cobra String pflag and associated viper config option
func StringP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName, cobraDefault, cobraDescription string) error {
	flagSet.StringP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringSlice registers a new cobra StringSlice flag and associated viper config option
func StringSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []string, cobraDescription string) error {
	flagSet.StringSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringSliceP registers a new cobra StringSlice pflag and associated viper config option
func StringSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []string, cobraDescription string) error {
	flagSet.StringSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToInt registers a new cobra StringToInt flag and associated viper config option
func StringToInt(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault map[string]int, cobraDescription string) error {
	flagSet.StringToInt(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToInt64 registers a new cobra StringToInt64 flag and associated viper config option
func StringToInt64(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault map[string]int64, cobraDescription string) error {
	flagSet.StringToInt64(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToInt64P registers a new cobra StringToInt64 pflag and associated viper config option
func StringToInt64P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault map[string]int64, cobraDescription string) error {
	flagSet.StringToInt64P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToIntP registers a new cobra StringToInt pflag and associated viper config option
func StringToIntP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault map[string]int, cobraDescription string) error {
	flagSet.StringToIntP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToString registers a new cobra StringToString flag and associated viper config option
func StringToString(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault map[string]string, cobraDescription string) error {
	flagSet.StringToString(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// StringToStringP registers a new cobra StringToString pflag and associated viper config option
func StringToStringP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault map[string]string, cobraDescription string) error {
	flagSet.StringToStringP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint registers a new cobra Uint flag and associated viper config option
func Uint(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault uint, cobraDescription string) error {
	flagSet.Uint(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint16 registers a new cobra Uint16 flag and associated viper config option
func Uint16(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault uint16, cobraDescription string) error {
	flagSet.Uint16(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint16P registers a new cobra Uint16 pflag and associated viper config option
func Uint16P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault uint16, cobraDescription string) error {
	flagSet.Uint16P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint32 registers a new cobra Uint32 flag and associated viper config option
func Uint32(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault uint32, cobraDescription string) error {
	flagSet.Uint32(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint32P registers a new cobra Uint32P flag and associated viper config option
func Uint32P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault uint32, cobraDescription string) error {
	flagSet.Uint32P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint64 registers a new cobra Uint64 flag and associated viper config option
func Uint64(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault uint64, cobraDescription string) error {
	flagSet.Uint64(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint64P registers a new cobra Uint64 pflag and associated viper config option
func Uint64P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault uint64, cobraDescription string) error {
	flagSet.Uint64P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint8 registers a new cobra Uint8 flag and associated viper config option
func Uint8(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault uint8, cobraDescription string) error {
	flagSet.Uint8(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// Uint8P registers a new cobra Uint8 pflag and associated viper config option
func Uint8P(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault uint8, cobraDescription string) error {
	flagSet.Uint8P(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// UintP registers a new cobra Uint pflag and associated viper config option
func UintP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault uint, cobraDescription string) error {
	flagSet.UintP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// UintSlice registers a new cobra UintSlice flag and associated viper config option
func UintSlice(flagSet *pflag.FlagSet, viperName, cobraName string, cobraDefault []uint, cobraDescription string) error {
	flagSet.UintSlice(cobraName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}

// UintSliceP registers a new cobra UintSlice pflag and associated viper config option
func UintSliceP(flagSet *pflag.FlagSet, viperName, cobraName, cobraShortName string, cobraDefault []uint, cobraDescription string) error {
	flagSet.UintSliceP(cobraName, cobraShortName, cobraDefault, descriptionWithConfig(cobraDescription, viperName))
	return viper.BindPFlag(viperName, flagSet.Lookup(cobraName))
}
