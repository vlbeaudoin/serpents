[![Go Reference](https://pkg.go.dev/badge/codeberg.org/vlbeaudoin/serpents.svg)](https://pkg.go.dev/codeberg.org/vlbeaudoin/serpents)

## vlbeaudoin/serpents

Go module built around spf13/pflag and spf13/viper to wrap around and simplify cobra-viper flag and config value duos declarations

It is licensed under the MIT license (see [LICENSE](LICENSE) for more details)

### Usage

Use in your go project with:

`$ go get codeberg.org/vlbeaudoin/serpents`

See the [Go reference](https://pkg.go.dev/codeberg.org/vlbeaudoin/serpents) for further documentation and examples
